// tslint:disable:no-console

import Debug from 'debug'
import { app } from './app'

const debug = Debug('server')

const port: number = app.get('port')
const env: number = app.get('env')

const server = app.listen(port, () => {
	console.log(`  App is running at http://localhost:${port} in ${env} mode`)
	console.log('  Press CTRL-C to stop\n')
})

// Handle sever exit
process.on('SIGINT', () => {
	console.log('\nShutting down')
	process.exit()
})

export { server }
