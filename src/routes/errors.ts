import Debug from 'debug'
import { NextFunction, Request, Response } from 'express'

const debug = Debug('route:error')

export const notFound = (req: Request, res: Response) => {
	res.status(404).send('Sorry can\'t find that!')
}

export const errPage = (err: Error, req: Request, res: Response) => {
	debug(err.stack)
	res.status(500).send('Something broke!')
}
