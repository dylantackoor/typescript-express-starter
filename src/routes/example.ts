import { RequestHandler, Router } from 'express'

const exampleRouter = Router()

const renderExample: RequestHandler = (req, res) => {
	const responseJSON: any = { status: true }
	responseJSON.pls = 'k'

	const input = req.param('input')
	if (input) {
		responseJSON.input = input
	}

	res.json(responseJSON)
}

exampleRouter.route('/').get(renderExample)

export { exampleRouter }
