import { RequestHandler, Router } from 'express'

// Create router
const homeRouter: Router = Router()

// Method functions
const renderHomepage: RequestHandler = (req, res) => {
	res.send('home')
}

// Attach method functions
homeRouter.route('/').get(renderHomepage)

// Export for use in app.ts
export { homeRouter }
