// Environment variables
const {
	NODE_ENV,
	PORT,
} = process.env;

export {
	NODE_ENV,
	PORT,
};
