import bodyParser from 'body-parser'
import compression from 'compression'
import errorHandler from 'errorhandler'
import express from 'express'
import morgan from 'morgan'
import { NODE_ENV, PORT } from './config/config'

// Importing Routers
import { errPage, notFound } from './routes/errors'
import { exampleRouter } from './routes/example'
import { homeRouter } from './routes/home'

// Creating server instance
const app = express()

// Server settings
app.set('port', PORT || 3000)

// Middleware
app.use(morgan('dev'))
app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
if (NODE_ENV !== 'production') {
	app.use(errorHandler())
}

// Routes
app.use('/', homeRouter)
app.use('/example', exampleRouter)
app.use(notFound)
app.use(errPage)

// Export server now that it's all set up
export { app }
